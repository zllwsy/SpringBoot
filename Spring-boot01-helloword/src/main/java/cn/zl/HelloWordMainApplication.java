package cn.zl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.swing.*;

/**
 * @author zhanglang
 * @Date 2020/3/22
 */
@SpringBootApplication
public class HelloWordMainApplication {
    public static void main(String[] args) {

        //启动Spring应用
        SpringApplication.run(HelloWordMainApplication.class,args);
    }
}

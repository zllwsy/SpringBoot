package cn.zl.springboot;


import cn.zl.springboot.bean.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

/*
* Springboot单元测试
* */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBoot02ConfigApplicationTests {
    @Autowired
    private Person person;

    @Autowired
    ApplicationContext ioc;
    @Test
    public void contextLoads() {
        System.out.println(person);
    }
    @Test
    public void testHelloService(){
        boolean b=ioc.containsBean("helloService");
        System.out.println(b);
    }
}
//public class Singleton{
//    private  Singleton(){
//
//    }
//    public static Singleton instanse=new Singleton();
//    public static Singleton getInstance(){
//        return instanse;
//    }
//}

package cn.zl.springboot.component;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

/**
 * 给容器中加入我们自己定义的错误属性
 *
 * @author zhanglang
 * @Date 2020/3/31
 */
@Component
public class MyErrorAttributes extends DefaultErrorAttributes {
    @Override
    //返回值的map就是页面和json能获取的所有字段
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
        Map<String, Object> map = super.getErrorAttributes(webRequest,includeStackTrace);
        map.put("company","zl");
        //异常处理器携带的数据
        Map<String, Object> ext = (Map<String, Object>)webRequest.getAttribute("ext", 0);
        map.put("ext",ext);
        return map;

    }

    public MyErrorAttributes() {
        super(true);
    }
}

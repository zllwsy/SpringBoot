package cn.zl.springboot.config;

import cn.zl.springboot.component.LoginHandlerIntercepter;
import cn.zl.springboot.component.MyLocaleResolver;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.*;

/**
 * @author zhanglang
 * @Date 2020/3/26
 */
@Configuration
//@EnableWebMvc 不要全面接管SpringMVC
public class MyMvcConfig implements WebMvcConfigurer {

    public WebServerFactoryCustomizer<TomcatServletWebServerFactory> webServerFactoryCustomizer(){
        return new WebServerFactoryCustomizer<TomcatServletWebServerFactory>() {
            //定制嵌入式的Servlet容器相关的规定
            @Override
            public void customize(TomcatServletWebServerFactory factory) {
                factory.setPort(8088);
            }
        };

    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry){
        //浏览器发送请求到Diamond
        registry.addViewController("/zl").setViewName("Diamond");
    }

    //所有的WebMvcConfigurerAdapter组件都会一起起作用
    @Bean //将组件注册在容器
    public WebMvcConfigurer webMvcConfigurer(){
        WebMvcConfigurer mc = new WebMvcConfigurer() {
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/").setViewName("login");
                registry.addViewController("/login.html").setViewName("login");
                registry.addViewController("/main.html").setViewName("dashboard");
            }
            //注册拦截器
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                //super.addInterceptors(registry);
                //静态资源；  *.css , *.js
                //SpringBoot已经做好了静态资源映射
//                registry.addInterceptor(new LoginHandlerIntercepter()).addPathPatterns("asserts/**","webjars/**")
//                        .excludePathPatterns("/index.html","/","/user/login","/static/");
            }
        };
        return mc;
    }
    @Bean
    public LocaleResolver localeResolver(){
        return new MyLocaleResolver();
    }
}

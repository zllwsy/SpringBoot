package cn.zl.springboot.controller;

import cn.zl.springboot.exception.UserNotExistException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Map;

/**
 * @author zhanglang
 * @Date 2020/3/24
 */
@Controller
public class HelloDiamond {

    @ResponseBody
    @RequestMapping("/hell")
    public String hell(@RequestParam("user")String user){
        if (user.equals("aaa")){
            throw new UserNotExistException();
        }
        return "Hello";
    }

    @RequestMapping({"/","/index.html"})
    public String index(){
        return "login";
    }
    @ResponseBody
    @RequestMapping("/hello")
    public String hello(){
        return "hello Diamond";
    }
    @RequestMapping("/Diamond")
    public String Diamond(Map<String,Object> map){
        map.put("hello","你好");
        map.put("users", Arrays.asList("zhangsan","lisi","wangwu"));
        return "Diamond";
    }
}

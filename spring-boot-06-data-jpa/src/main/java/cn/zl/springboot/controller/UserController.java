package cn.zl.springboot.controller;

import cn.zl.springboot.entity.User;
import cn.zl.springboot.repository.Userrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author zhanglang
 * @Date 2020/4/3
 */
@RestController
public class UserController {
    @Autowired
    Userrepository userrepository;

    @GetMapping("/user/{id}")
    public User getUser(@PathVariable("id")Integer id){
        User user = userrepository.findById(id).orElse(null);
        return user;
    }

    @GetMapping("/user")
    public User insertUser(User user){
        User save = userrepository.save(user);
        return save;
    }
}

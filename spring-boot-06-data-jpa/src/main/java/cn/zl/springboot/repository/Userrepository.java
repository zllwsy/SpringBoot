package cn.zl.springboot.repository;

import cn.zl.springboot.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhanglang
 * @Date 2020/4/3
 */
//继承JpaRepository来完成对数据库的操作
public interface Userrepository extends JpaRepository<User,Integer> {

}

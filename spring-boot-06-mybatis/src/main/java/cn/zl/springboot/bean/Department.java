package cn.zl.springboot.bean;

import org.springframework.stereotype.Repository;

/**
 * @author zhanglang
 * @Date 2020/4/3
 */

public class Department {
    private Integer id;
    private String departmentName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
}

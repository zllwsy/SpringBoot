package cn.zl.springboot.mapper;

import cn.zl.springboot.bean.Department;
import org.apache.ibatis.annotations.*;

/**
 * @author zhanglang
 * @Date 2020/4/3
 */
//指定这是一个操作数据库的Mapper
@Mapper
public interface DepartmentMapper {
    @Select("select * from department where id=#{id}")
    public Department getDeptById(Integer id);

    @Delete("delete from department where id=#{id}")
    public int deleteDeptById(Integer id);

    @Options(useGeneratedKeys = true,keyProperty = "id")
    @Insert("insert into department(department_name) value(#{departmentName})")
    public int insertDept(Department department);

    @Update("update department set department_name=#{departmentName} where id=#{id}")
    public int updateDept(Department department);
}

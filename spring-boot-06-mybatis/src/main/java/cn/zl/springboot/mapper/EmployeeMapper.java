package cn.zl.springboot.mapper;

import cn.zl.springboot.bean.Employee;

/**
 * @author zhanglang
 * @Date 2020/4/3
 */
//@Mapper或者@MapperScan将接口扫描装配到容器中
public interface EmployeeMapper {
    public Employee getEmpById(Integer id);
    public void insertEmp(Employee employee);
}

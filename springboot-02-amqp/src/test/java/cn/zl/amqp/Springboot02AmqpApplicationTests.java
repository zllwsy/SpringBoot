package cn.zl.amqp;

import cn.zl.amqp.bean.Book;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestReporter;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import sun.plugin2.message.Message;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Exchanger;

@SpringBootTest
class Springboot02AmqpApplicationTests {
    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    AmqpAdmin amqpAdmin;

    @Test
    public void createExchange(){
//        amqpAdmin.declareExchange(new DirectExchange("amqpAdmin.exchange"));
//        System.out.println("交换器创建完成");

//        amqpAdmin.declareQueue(new Queue("amqpAdmin.queue",true));
//        System.out.println("消息队列创建完成");

        amqpAdmin.declareBinding(new Binding("amqpAdmin.queue", Binding.DestinationType.QUEUE,
                "amqpAdmin.exchange","amqp.haha",null));
        System.out.println("绑定创建完成");

//        amqpAdmin.deleteExchange();删除
//        amqpAdmin.deleteQueue();删除
    }

    //单播（点对点）
    @Test
    public void contextLoads() {
         //Message需要自己构造一个;定义消息体内容和消息头
        //rabbitTemplate.send(exchage,routeKey,message);

        //object默认当成消息体，只需要传入要发送的对象，自动序列化发送给rabbitmq；
        //rabbitTemplate.convertAndSend(exchage,routeKey,object);
        Map<String,Object> map=new HashMap<>();
        map.put("msg","这是第一个消息");
        map.put("data", Arrays.asList("helloworld",123, true));
        //对象被默认序列化以后发送出去
//        rabbitTemplate.convertAndSend("exchange.direct","zl.news",map);
        rabbitTemplate.convertAndSend("exchange.direct","zl.news",new Book("西游记","吴承恩"));
    }

    //接受数据，如果将数据转换为JSON发送出去
    @Test
    public void receive(){
        Object o = rabbitTemplate.receiveAndConvert("zl.news");
        System.out.println(o.getClass());
        System.out.println(o);
    }

    //广播
    @Test
    public void sendMsg(){
        rabbitTemplate.convertAndSend("exchange.fanout","",
                new Book("红楼梦","曹雪芹"));
    }

}

package cn.zl.task.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author zhanglang
 * @Date 2020/4/10
 */
@Service
public class AsyncService {

    /**
     * 告诉Spring这是异步的
     */
    @Async
    public void hello(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("处理数据中...");
    }
}

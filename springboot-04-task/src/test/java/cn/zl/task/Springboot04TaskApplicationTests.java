package cn.zl.task;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;
import javax.swing.text.html.HTML;
import java.io.File;

@SpringBootTest
class Springboot04TaskApplicationTests {

    @Autowired
    JavaMailSenderImpl javaMailSender;
    @Test
    public void contextLoads() {
        SimpleMailMessage message=new SimpleMailMessage();
        message.setSubject("通知-今晚通宵");
        message.setText("今晚吃鸡");

        message.setTo("zllwsy@outlook.com");
        message.setFrom("zllwsy@163.com");
        javaMailSender.send(message);
    }

    @Test
    public void test02() throws Exception{
        //1.创建一个复杂的消息邮件
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        //2.复杂邮件需要邮件助手
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true,"UTF-8");
        //3.邮件设置
        helper.setSubject("通知-今晚通宵");
        helper.setText("<b style='color:red;font:15'>今晚七点开始全城吃鸡</b>", true);

        helper.setTo("zllwsy@outlook.com");
        helper.setFrom("zllwsy@163.com");

        //4.上传文件
//        String path1="‪D:\\sign\\Rate.jpg";
//        String filePath1=path1.replace("\\\\","/");
        helper.addAttachment("Rate.jpg",new File("D:\\sign\\Rate.jpg"));
//        helper.addAttachment("image.jpg",new File("\u202AD:\\sign\\image.jpg"));
        javaMailSender.send(mimeMessage);
    }

}

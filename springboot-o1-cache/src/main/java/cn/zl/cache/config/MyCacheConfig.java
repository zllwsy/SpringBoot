package cn.zl.cache.config;

import cn.zl.cache.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @author zhanglang
 * @Date 2020/4/5
 */
@Configuration
public class MyCacheConfig {
    @Autowired
    EmployeeMapper employeeMapper;
    @Bean("myGenerator")
    public KeyGenerator keyGenerator(){
        return new KeyGenerator() {
            @Override
            public Object generate(Object o, Method method, Object... params) {
                return method.getName()+"{"+ Arrays.asList(params).toString()+"}";
            }

        };
    }
}

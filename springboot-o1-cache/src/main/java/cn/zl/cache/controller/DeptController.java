package cn.zl.cache.controller;

import cn.zl.cache.bean.Department;
import cn.zl.cache.mapper.DepartmentMapper;
import cn.zl.cache.service.DeptService;
import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhanglang
 * @Date 2020/4/5
 */
@RestController
public class DeptController {

    @Autowired
    DeptService deptService;
    @GetMapping("/dept/{id}")
    public Department getDept(@PathVariable("id") Integer id){
        return deptService.getDeptById(id);

    }
}

package cn.zl.cache.service;


import cn.zl.cache.bean.Department;
import cn.zl.cache.mapper.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zhanglang
 * @Date 2020/4/5
 */
@Service
public class DeptService {
    @Autowired
    DepartmentMapper departmentMapper;
//    @Qualifier("MyredisCacheManager")
    @Autowired
    RedisCacheManager MyredisCacheManager;
//    @Cacheable(cacheNames = "dept")
//    public Department getDeptById(Integer id) {
//        System.out.println("查询部门"+id);
//        Department department=departmentMapper.getDeptById(id);
//        return department;
//    }
    public Department getDeptById(Integer id) {
        System.out.println("查询部门"+id);
        Department department=departmentMapper.getDeptById(id);
        Cache dept = MyredisCacheManager.getCache("dept");
        dept.put("dept:1",department);
        return department;
}

}
